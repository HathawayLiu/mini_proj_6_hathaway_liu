use lambda_runtime::{service_fn, Context, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use tracing::{info, instrument};
use tracing_subscriber::fmt::format::FmtSpan;

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_span_events(FmtSpan::CLOSE)
        .init();

    let func = service_fn(func);
    lambda_runtime::run(func).await?;
    Ok(())
}

#[derive(Debug, Deserialize)]
struct CustomEvent {
    num1: i64,
    num2: i64,
}

#[derive(Serialize)]
struct CustomOutput {
    gcd: i64,
}

#[instrument]
async fn func(event: LambdaEvent<CustomEvent>) -> Result<CustomOutput, Error> {
    let (event, _context) = event.into_parts();
    info!("Received event: {:?}", event);
    let gcd = gcd(event.num1, event.num2);
    Ok(CustomOutput { gcd })
}

// Function to calculate the GCD using Euclid's algorithm
fn gcd(mut a: i64, mut b: i64) -> i64 {
    while b != 0 {
        let t = b;
        b = a % b;
        a = t;
    }
    a.abs() // Return the absolute value in case of negative numbers
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_lambda_function() {
        let test_event = CustomEvent { num1: 54, num2: 24 };
        let context = Context::default();

        let response = func(LambdaEvent::new(test_event, context)).await;
        assert!(response.is_ok());
        let output = response.unwrap();
        assert_eq!(output.gcd, 6); // The GCD of 54 and 24 is 6
    }
}
