# Mini Project 6: Rust Lambda with Tracing and Logging

## Project Description
This project involves enhancing a Rust-based AWS Lambda function with advanced logging and tracing capabilities, specifically targeting the integration with AWS services such as `AWS X-Ray` and `CloudWatch`. The primary goal is to ensure that the Lambda function not only performs its computational tasks efficiently but also provides comprehensive insights into its execution, error handling, and performance metrics.

## Function Description
For this specific project, I choose to write a simple Lambda function that takes in two arguments:
```
num1: interger
num2: interger
```
and returns their GCD(Greatest Common Divisor) as result.

## Get started with Cargo Lambda
1. You can refer to the official [Cargo Lambda website](https://www.cargo-lambda.info/guide/getting-started.html) to get your hands on installing Cargo lambda on your own device.
2. Then you can run `cargo lambda new <Your own project name>` to create your own project. Make sure that you replace your own project name inside.
3. Go to `main.rs` and start to write your own lambda function. Make sure to add the corresponding dependencies in `Cargo.toml`.
4. After writing your contend, you can use the following command to test your function:
```shell
cargo fmt --quiet
cargo clippy --quiet
cargo test --quiet
```
5. Without any error, you can run `cargo lambda watch` in one terminal; then open a new terminal and use `cargo lambda invoke --data-ascii "{ \"command\": \"encrypt\", \"message\": \"encrypt\" }"` to test if your function is working. Then you can run `cargo lambda build --release --arm64` to build your function.

## Detail Steps
### Preparation Steps
1. Like what we did in other projects, create a user under `AWS IAM`. Add permissions to the following policies: `iamfullaccess`, `lambdafullaccess`, `AmazonXrayFullAccess`. Then, under Secruity Credentials, generate a new `Access Key` and store it in somewhere save.
2. Go to your own terminal, use `cargo lambda new <Your Project Name>` to create a new project 

### Implementation Steps
1. Go to your `main.rs` and starts to write your own function.
2. Use necessary header and add the corresponding dependencies in `Cargo.toml`. To add logging and tracing to a Rust Lambda function, make sure to add the following dependencies: `tracing`, `tracing-subscriber`. You can refer to mine for help.
3. Add a corresponding `Makefile`. You can download mine and use it. Make sure to change the `invoke` command to match your own function.
4. Now, run `cargo lambda watch` to allow the function running. Open another terminal, run `cargo lambda invoke --data-ascii "{ \"command\": \"encrypt\", \"message\": \"encrypt\" }"` to test if your function gives the correct output. 
5. Create a `.env` file and add your two access keys and access region to the file with the following structure:
```plaintext
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_REGION=
```
Then create a `.gitignore` file:
```plaintext
/target
.env
```
Then use either `Export` or `set -a` and `source .env` to allow your terminal to detect your AWS user

6. Then run `cargo lambda build` and `cargo lambda deploy` to deploy your lambda function.
7. Go to `Lambda` in AWS Console, check if your function is already on there. Then go to `Configuration` and go to `Monitoring and Operation tools`. Click on edit to turn on Active tracing for `AWS X-Ray` and Enhanced monitoring for `CloudWatch Lambda Insights`. 
8. Then you can either test your function on `AWS Lambda` by go to `Test` Section, or simiply use `cargo lambda invoke --remote mini-proj-6 --data-ascii "{ \"num1\": 54, \"num2\": 24}"` to test it remotely.(**Remember to change the command based on your function.**)
9. After successfully testing, go to the `Monitor` Section and scroll down to the bottom. You will be able to see all the `Traces`. Click on one of them, it will navigate you to `AWS CloudWatch`. You should be able to find all the trace details and logs info. 
10. Now, go to `API Gateway`, create a `REST API` with the stage you create. Then create a method under it. Make sure you select type `ANY`. Finally you can deploy your API and you will be able to see a API like the following: https://hrwpw12b50.execute-api.us-east-1.amazonaws.com/mini6
11. You can test your API using following command:
```
curl -X POST https://hrwpw12b50.execute-api.us-east-1.amazonaws.com/mini6/ \
  -H 'content-type: application/json' \
  -d '{ "num1": 54, "num2": 24}'
```
make sure to replace it with your own API and own command.

## Deliverables
- Successful Invoke and Error Invoke in local terminal
![Successful Invoke](https://gitlab.com/HathawayLiu/mini_proj_6_hathaway_liu/-/wikis/uploads/654181f674fcc122290f07d33eed9bf3/Screenshot_2024-03-03_at_5.15.47_PM.png)
![Error Invoke](https://gitlab.com/HathawayLiu/mini_proj_6_hathaway_liu/-/wikis/uploads/3d8cf1b83253e4f4c5a67fb81feedbba/Screenshot_2024-03-03_at_5.16.29_PM.png)
- CloudWatch and X-Ray
![](https://gitlab.com/HathawayLiu/mini_proj_6_hathaway_liu/-/wikis/uploads/9afb9f5a108611a77d9693985e5c7efb/Screenshot_2024-03-03_at_5.49.47_PM.png)
- Tracing details and logging info
![](https://gitlab.com/HathawayLiu/mini_proj_6_hathaway_liu/-/wikis/uploads/eae1906261082a6347a8fe3ff7eccd56/Screenshot_2024-03-03_at_8.41.46_PM.png)
![](https://gitlab.com/HathawayLiu/mini_proj_6_hathaway_liu/-/wikis/uploads/48393de2184c9592675a4de08483aa23/Screenshot_2024-03-03_at_8.41.21_PM.png)
